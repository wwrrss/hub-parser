/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ef;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author william
 */
public class DbConnector {

    public static String user = "root";
    public static String pass = "";
    public static String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
    public static String host = "localhost";
    public static String port = "3306";
    public static String db = "hub-db";
    private Connection connection;
    private PreparedStatement insertPreparedStatement;

    public DbConnector() throws ClassNotFoundException, SQLException {
        getConnection();
    }

    private void getConnection() throws ClassNotFoundException, SQLException {
        Class.forName(JDBC_DRIVER);
        Properties properties = new Properties();
        properties.setProperty("user", user);
        properties.setProperty("password", pass);
        properties.setProperty("useSSL", "false");
        String connectionUrl = new StringBuilder()
                .append("jdbc:mysql://")
                .append(host)
                .append(":")
                .append(port)
                .append("/")
                .append(db)
                .toString();
   

        connection = DriverManager.getConnection(connectionUrl, properties);
      
    }

    public void insertLog(LogModel logModel) throws SQLException {
        if (insertPreparedStatement == null) {
            String sql = "INSERT INTO requests (id_request,request_date, ip_address,request_method, http_status,user_agent) values (0,?,?,?,?,?)";
            insertPreparedStatement = this.connection.prepareStatement(sql);
        }
        
        insertPreparedStatement.clearParameters();
        insertPreparedStatement.setTimestamp(1, new Timestamp(logModel.getDate().getTime()));
        insertPreparedStatement.setString(2, logModel.getIp());
        insertPreparedStatement.setString(3, logModel.getMethod());
        insertPreparedStatement.setInt(4, logModel.getHttpStatus());
        insertPreparedStatement.setString(5, logModel.getUserAgent());
        insertPreparedStatement.execute();

    }
    
    
    public void findIps(java.util.Date startDate, java.util.Date endDate, Integer threshold, String rate){
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            String sql = "select count(*) as nreq, ip_address from requests where request_date \n" +
                    "between ? and ? group by ip_address\n" +
                    "having nreq > ? ";
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setTimestamp(1, new Timestamp(startDate.getTime()));
            preparedStatement.setTimestamp(2, new Timestamp(endDate.getTime()));
            preparedStatement.setInt(3, threshold);
            
            rs = preparedStatement.executeQuery();
            while(rs.next()){
                Integer count = rs.getInt("nreq");
                String ip = rs.getString("ip_address");
                String cause = new StringBuilder()
                                    .append("This Ip has been blocked, ")
                                    .append("made ")
                                    .append(count)
                                    .append(" ")
                                    .append(rate).toString();
                insertBlockedIps(ip, cause);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DbConnector.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            if(preparedStatement!=null){
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DbConnector.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            
            if(rs!=null){
                try {
                    rs.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DbConnector.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    private void insertBlockedIps(String ip, String cause){
        System.out.println(ip);
        String sql = "insert into blocked_ip (id_bloqued, ip_address, cause) values (0,?,?)";
        PreparedStatement preparedStatement = null;
        try{
            preparedStatement = this.connection.prepareStatement(sql);
            preparedStatement.setString(1, ip);
            preparedStatement.setString(2, cause);
            preparedStatement.execute();
            
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            if(preparedStatement!=null){
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DbConnector.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        
                
        
    }
    
    
    public void closeConnection(){
        if(insertPreparedStatement!=null){
            try {
                insertPreparedStatement.close();
            } catch (SQLException ex) {
                Logger.getLogger(DbConnector.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        if(this.connection!=null){
            try {
                this.connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(DbConnector.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }

}
