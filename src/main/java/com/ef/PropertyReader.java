/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ef;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author william
 */
public class PropertyReader {
    public void loadProperties(){
            InputStream inputStream = null;
        try {
            String workFolder = System.getProperty("user.dir");
            System.out.println(workFolder);
            Properties properties = new Properties();
        
            inputStream = new FileInputStream("config.properties");
            properties.load(inputStream);
            DbConnector.user = properties.getProperty("user");
            DbConnector.pass = properties.getProperty("pass");
            DbConnector.host = properties.getProperty("host");
            DbConnector.port = properties.getProperty("port");
            DbConnector.db = properties.getProperty("db");
        } catch (IOException ex) {
       
        }finally{
            if(inputStream != null){
                try {
                    inputStream.close();
                } catch (IOException ex) {
                    Logger.getLogger(PropertyReader.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        
    }
}
