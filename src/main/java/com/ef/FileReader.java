/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ef;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author william
 */
public class FileReader {

    private DbConnector dbConnector;
    private SimpleDateFormat sdf;
    private String logFilePath;

    public FileReader(DbConnector dbConnector,String logFilePath) {
        this.dbConnector = dbConnector;
        this.logFilePath = logFilePath;
        this.sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
    }

    public void readFile() {
        BufferedReader br = null;

        try {
            br = new BufferedReader(new java.io.FileReader(logFilePath));
            String curLine = null;
            LogModel logModel;
            while ((curLine = br.readLine()) != null) {

                String[] row = curLine.split("\\|");

                if (row.length == 5) {
                    logModel = new LogModel(sdf.parse(row[0]), row[1], row[2], Integer.valueOf(row[3]), row[4]);
                    this.dbConnector.insertLog(logModel);
                }
            }

        } catch (Exception e) {
           e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException ex) {
                    Logger.getLogger(FileReader.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

}
