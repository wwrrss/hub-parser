/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ef;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author william
 */
public class Parser {
    
    public static Date givenDate;
    public static Date endDate;
    public static String givenDuration;
    public static Integer givenThreshold;
    public static String logFilePath;
    
    public static void main(String[] args) throws ParseException{
    
        DbConnector dbConnector = null;
        PropertyReader propertyReader = new PropertyReader();
        propertyReader.loadProperties();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss");
        
        if(args!=null){
            for(int i=0;i<args.length;i++){
                
                String[] splitArg = args[i].split("\\=");
                if(splitArg.length==2){
                    switch(splitArg[0]){
                        case "--startDate":
                            givenDate = sdf.parse(splitArg[1]);
                            break;
                        case "--duration":
                            givenDuration = splitArg[1];
                            break;
                        case "--threshold":
                            givenThreshold = Integer.valueOf(splitArg[1]);
                            break;
                        case "--accesslog":
                            logFilePath= splitArg[1];
                            break;
                    }
                }
                
            }
        }
        
        if(givenDuration!=null){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(givenDate);
            switch(givenDuration){
                case "hourly":
                    calendar.add(Calendar.HOUR_OF_DAY, 1);
                    endDate = calendar.getTime();
                    break;
                    
                case "daily":
                    calendar.add(Calendar.DAY_OF_MONTH,1);
                    endDate = calendar.getTime();
                    break;
            
            }
        }

        try {
            dbConnector = new DbConnector();
            if(logFilePath!=null){
                FileReader fileReader = new FileReader(dbConnector,logFilePath);
                fileReader.readFile();
            }
            dbConnector.findIps(givenDate, endDate, givenThreshold,givenDuration);
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(Parser.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            if(dbConnector!=null)
                dbConnector.closeConnection();
        }
    
    }
}
