/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ef;

import java.util.Date;



/**
 *
 * @author william
 */
public class LogModel {
    private Date date;
    private String ip;
    private String method;
    private int httpStatus;
    private String userAgent;
    
    public LogModel(){
        
    }

    public LogModel(Date date, String ip, String method, int httpStatus, String userAgent) {
        this.date = date;
        this.ip = ip;
        this.method = method;
        this.httpStatus = httpStatus;
        this.userAgent = userAgent;
    }
    
    
    

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public int getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(int httpStatus) {
        this.httpStatus = httpStatus;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    @Override
    public String toString() {
        return "LogModel{" + "date=" + date + ", ip=" + ip + ", method=" + method + ", httpStatus=" + httpStatus + ", userAgent=" + userAgent + '}';
    }
    
    
}
